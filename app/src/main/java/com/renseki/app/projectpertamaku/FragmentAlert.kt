package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.renseki.app.projectpertamaku.model.DataUser
import kotlinx.android.synthetic.main.fragment_alert.*

class FragmentAlert : Fragment() {
    companion object {
        fun newInstance() = FragmentAlert()
    }

    override fun onCreateView(inflater: LayoutInflater
                              , container: ViewGroup?
                              , savedInstanceState: Bundle?)
            : View? {
        return inflater!!.inflate(R.layout.fragment_alert,container,false)
    }

    fun ReceiveErrorMessage(DataUser : DataUser){
        if (DataUser.username.equals("") || DataUser.password.equals("")) {
            alert.append(getString(R.string.error_password_dan_username)+System.getProperty("line.separator"))
        } else {
            if (!DataUser.password.equals("stts")) {
                alert.append(getString(R.string.error_password_harus_stts)+System.getProperty("line.separator"))
            } else {
                Toast.makeText(this.context,DataUser.username, Toast.LENGTH_SHORT).show()
                val i = WelcomeActivity.getStartIntent(
                        requireContext(),
                        DataUser(
                                DataUser.username,
                                DataUser.password
                        )
                )
                startActivity(i)
            }
        }
    }
}