package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.projectpertamaku.model.DataUser

class ActivityLogin : AppCompatActivity(), FragmentLogin.LoginInputActionListener {
    private lateinit var alertFragment: FragmentAlert

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        fragmentLogin()
        fragmentAlert()
    }

    private fun fragmentAlert() {
        alertFragment = FragmentAlert.newInstance()

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_2,
                alertFragment
        )
        fragmentTransaction.commit()
    }

    private fun fragmentLogin() {
        val fragment = FragmentLogin.newInstance(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }

    override fun onDataUserRady(DataUser: DataUser) {
        alertFragment.ReceiveErrorMessage(DataUser)
    }
}