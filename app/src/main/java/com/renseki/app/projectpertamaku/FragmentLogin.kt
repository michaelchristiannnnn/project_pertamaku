package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.renseki.app.projectpertamaku.model.DataUser
import kotlinx.android.synthetic.main.fragment_login.*

class FragmentLogin : Fragment() {
    private  lateinit var listener: LoginInputActionListener

    interface LoginInputActionListener {
        fun onDataUserRady(DataUser : DataUser)
    }

    companion object {
        fun newInstance(listener: LoginInputActionListener): FragmentLogin {
            val fragment = FragmentLogin()
            fragment.listener = listener
            return fragment
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.fragment_login,
                container,
                false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_login.setOnClickListener {
            val username = username.text.toString()
            val pass = password.text.toString()
            sendToOtherFragment(username,pass)
        }
    }

    private fun sendToOtherFragment(username: String, pass: String) {
        val DataUser = DataUser(
                username,pass
        )
        listener.onDataUserRady(DataUser)
    }

}